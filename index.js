const app = require('./config/server');
const home = require('./app/routes/home');
const reserva = require('./app/routes/reservaRoutes');

home.home(app);
home.cadastroUsuario(app);
home.logarUsuario(app);
home.login(app);
home.cadastroUS(app);
reserva.atualizarReserva(app);
reserva.cancelarReserva(app);
reserva.usReservas(app);
reserva.telaReservas(app);
reserva.salvarReserva(app);
reserva.carrinhoReserva(app);