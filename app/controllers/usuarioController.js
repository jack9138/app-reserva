const UsuarioModel = require("../models/usuarios");

module.exports = class UsuarioController {
   static async buscarUsuario(req, res, next) {
      try {
         const usuario = await UsuarioModel.buscarUsuario(req);
         if (!usuario) {
            return null;
         }
         return usuario;
      } catch (error) {
         return error;
      }
   };
   static async cadastrarUsuario(req, res, next) {
      try {

         const usuario = await UsuarioModel.cadastrarUsuario(req);
         if (!usuario) {
            return null;
         }
         return usuario;
      } catch (error) {
         return error;
      }
   };
  
}