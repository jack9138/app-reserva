const ReservaModel = require("../models/reservas");
module.exports = class ReservasController {

   static async listaTiposReservas() {
      try {
         const tiposReservas = await ReservaModel.listarTiposReservas();
         if (!tiposReservas) {
            return(`Não existe reserva cadastrado.`);
         }
         return tiposReservas;
      } catch (error) {
         return error;
      }
   };
   static async buscarReservaPorId(reservaId) {
      try {
         const tiposReservas = await ReservaModel.listarTiposReservas();
         const reserva = tiposReservas.find(x => x._id == reservaId)
         if (!reserva) {
            return(`Não existe reserva cadastrado.`);
         }
         return reserva;
      } catch (error) {
         return error;
      }
   };
   static async listaReservasUsuario(userId) {
      try {

         const reservas = await ReservaModel.listaReservasUsuario(userId);
         
         if (!reservas) {
            return null;
         }
         return reservas;
      } catch (error) {
         return error;
      }
   };
   static async salvarReserva(reserva) {
      try {
         
         const tiposReservas = await ReservaModel.salvarReserva(reserva);
      
         if (!tiposReservas) {
            return(`Não existe reserva cadastrado.`);
         }
         return reserva;
      } catch (error) {
         return error;
      }
   };
   static async atualizarReserva(req){
      try{
         const atualziarReserva = await ReservaModel.atualizarReserva(req);
         return atualziarReserva;
      }catch(error){
         return error;
      }
  };
   static async cancelarReserva(id){
      try{
        
         const cancelarReserva = await ReservaModel.cancelarReserva(id);
         return cancelarReserva;

      }catch(error){
         return error;
     }
   }
}