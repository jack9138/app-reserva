const client = require('../../config/dbConnection');

module.exports = class ReservaModel {

    static async listarTiposReservas() {
        const cursor = await client.db("dsw").collection("typesreservations").find();
        
        const tiposReservas = await cursor.toArray();
        return tiposReservas;
    };
    static async listaReservasUsuario(userK) {
        
        const idUsuario = userK[0];
        const reservationsCollection = await  client.db("dsw").collection("reservations").find({idUsuario});
        const reservas = await reservationsCollection.toArray();
        
        return reservas;
    };
    static async salvarReserva(reserva) {

         const reservaM = {
            idReserva: reserva.idReserva,
            dataReserva: reserva.dataReserva,
            idUsuario: reserva.idUsuario,
            statusId: reserva.statusId,
            NameType: reserva.NameType
        }
        
        const collection = await client.db("dsw").collection("reservations").insertOne(reservaM);
        if (collection > 0) {
           return collection;
        } else {
            return('Falha ao salvar a reserva.');
        }
    };
    static async atualizarReserva(data) {
        const filter = { _id: data.idReserva };
        const update = {
            $set: {
                dataReserva: data.dataReserva
            }
        }
       const collection = await client.db("dsw").collection("reservations").update(filter,update);
       if (collection > 0) {
          return collection;
       } else {
           return('Falha ao salvar a reserva.');
       }
    };
    static async cancelarReserva(id) {
        const filter = { _id: id };

        const collection = await client.db("dsw").collection("reservations").deleteOne(filter);
        if (collection > 0) {
            return collection;
        } else {
            return('Falha ao salvar a reserva.');
        }
    };
}