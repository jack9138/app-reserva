const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Defina o esquema da reserva
const reservaSchema = new Schema({
    idReserva: String,
    dataReserva: Date,
    idUsuario: String,
    statusId: Number,
    NameType: String
});

// Crie um modelo de reserva com base no esquema
const Reserva = mongoose.model('Reserva', reservaSchema);

module.exports = Reserva; // Exporte o modelo de reserva
