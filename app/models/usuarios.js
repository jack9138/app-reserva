const client = require('../../config/dbConnection');

module.exports = class ReservaModel {

    static async buscarUsuario(req) {
        
        const cursor = await client.db("dsw").collection("users").find();
        const usuarios = await cursor.toArray();
        
        const usuario = usuarios.find(x => x.UserName === req.username && x.UserSenha === req.usersenha);
        return usuario;
    };
    static async cadastrarUsuario(req) {
       
        const user = await client.db("dsw").collection("users").insertOne(req);
        
        return user;
    };
}