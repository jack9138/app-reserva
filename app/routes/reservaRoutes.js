const Reserva = require("../controllers/reservaController");
const ReservaM = require('../models/mongo/models/reservaModel'); 
const home = require('./home');

module.exports ={ 
    telaReservas: (app) => {
        app.get('/reservastipos', async function (req, res) { 
            try {
                
                const tiposReservas = await Reserva.listaTiposReservas();
                if (!tiposReservas) {
                    res.json('Não existe reserva cadastrado.');
                } else {
                    res.render('listaReservas', { tiposReservas: tiposReservas });
                }
            } catch (error) {
                console.log(error);
                res.json({ error: error });
            }
        })
    },
    usReservas: (app) => {
        app.get('/minhasReservas', async function (req, res) { 
            try {
                const userK = home.userCache.keys();
                if(userK != 0){

                    const reservas = await Reserva.listaReservasUsuario(userK);

                    
                    if (!reservas) {
                        res.json('Não existe reserva cadastrado.');
                    } else {
                        res.render('usuarioReservas', { reservas: reservas });
                    }
                }else{
                    res.redirect('/login');
                }
                
            } catch (error) {
                console.log(error);
                res.json({ error: error });
            }
        })
    },
    carrinhoReserva: (app) => {
        app.get('/carrinho/:id', async function (req, res) {
            try {
                
                const reservaId = req.params.id;
                const reservaSelecionada = await Reserva.buscarReservaPorId(reservaId);
                
                
                if (!reservaSelecionada) {
                    res.json('Reserva não encontrada.');
                } else {
                    const chavesNoCache = home.userCache.keys();
                    if(chavesNoCache == 0){
                        
                        const reservaInfo = {
                            id:  reservaSelecionada._id,
                            username:  reservaSelecionada.NameType
                        };
                        
                        home.reservaCache.set(`${reservaInfo.id}`, reservaInfo, 3600);
                        
                        res.redirect('/login');
                    }else{
                      
                        res.render('telaCarrinhoCompra', { reservaSelecionada: reservaSelecionada });
                    }
                }
            } catch (error) {
                console.log(error);
                res.json({ error: error });
            }
        });
    },
    salvarReserva: (app) => {
        app.post('/salvarReserva',async function (req, res) {
            try {
                const userId = home.userCache.keys();
                
                const reserva = new ReservaM({
                    idReserva: req.body.idReserva,
                    dataReserva: new Date(req.body.dataReserva),
                    idUsuario: `${userId}`, 
                    statusId: 2,
                    NameType: req.body.nameType
                });

                const salvarReserva = await Reserva.salvarReserva(reserva);
        
                const reservaCadastrada = {
                    NameType : req.body.NameType ,
                    TypeDescription : req.body.TypeDescription
                }

                if (!salvarReserva) {
                    res.json('Não foi possivel finalizar reserva!');
                } else {
                    res.render('telaCompraFinalizada', { reservaCadastrada: reservaCadastrada });
                }
            } catch (error) {
                console.log(error);
                res.json({ error: error });
            }
        });
    },
    cancelarReserva:(app)=>{
        app.get('/cancelarReserva/:id',async function (req, res) {
            try {
                const cancelarReserva = await Reserva.cancelarReserva(req.params.id);

                if (!cancelarReserva) {
                    res.json('Não foi possivel finalizar reserva!');
                } else {
                    res.redirect('/');
                }
            } catch (error) {
               
                res.json({ error: error });
            }
        });
    },
    atualizarReserva:(app)=>{
        app.post('/atualizarReserva',async function (req, res) {
            try {
                const atualizarReserva = await Reserva.atualizarReserva(req.body);
        
                if (!atualizarReserva) {
                    res.json('Não foi atualizar eserva!');
                } else {
                    res.redirect('/');
                }
            } catch (error) {
               
                res.json({ error: error });
            }
        });
    }
}