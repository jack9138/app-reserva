const Usuario = require("../controllers/usuarioController");
const NodeCache = require("node-cache");
const userCache = new NodeCache();
const reservaCache = new NodeCache();

module.exports ={
    userCache, 
    reservaCache,  
    home: (app) => {
        app.get('/',function(req,res){
            res.render('home.ejs')
        });
    },
    cadastroUsuario: (app) => {
        app.post('/cadastrarUsuario',async function(req,res){
            try{
                const usuario = {
                    UsuarioEmail: req.body.UsEmail,
                    UsuarioNome: req.body.UsNome,
                    UsuarioSenha: req.body.UsSenha
                }
                
                const cadUsuario = await Usuario.cadastrarUsuario(usuario)
                if(!cadUsuario){
                    res.render('cadastro.ejs');
                }else{
                    res.render('login.ejs');
                }
            } catch (error) {
                
                res.json({ error: error });
            }
           
        });
    },
    logarUsuario: (app) => {
        app.post('/logar',async function(req,res){
            try{
                userCache.close();
                const logado = await Usuario.buscarUsuario(req);
                const userInfo = {
                    id: logado._id,
                    username: logado.UsuarioNome
                };

                userCache.set(`${userInfo.id}`, userInfo, 360);
                if(!logado){
                    res.render('cadastro.ejs');
                }else{
                    if(reservaCache.keys() == 0){
                        res.redirect('/reservastipos');
                    }else{
                        const reservaId = reservaCache.keys();
                        res.redirect(`/carrinho/${reservaId}`);
                    }
                    
                }
            } catch (error) {
                res.json({ error: error });
            }

        });
    },
    login: (app) => {
        app.get('/login',function(req,res){
            
            const reservaId = reservaCache.keys();
            if(!reservaId == 0){
                res.render('login.ejs');
            }else{
                res.redirect(`/carrinho/${reservaId}`);
            }
        });
    },
    cadastroUS: (app) => {
        app.get('/cadastrarUS',function(req,res){
            res.render('cadastro.ejs');
        });
    }
}