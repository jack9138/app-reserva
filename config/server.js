console.log('[Config] Server');
let express = require('express');
const path = require('path');


let app = express(); 
let port = 3000;

app.set('view engine', 'ejs'); 
app.set('views', path.join(__dirname, '..', 'app', 'views'));


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, function () {
	console.log('Servidor rodando com express na porta', port);
});

module.exports = app;